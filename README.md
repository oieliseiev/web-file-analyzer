# Web file analyzer #

## Installation dependencies ##

* Java 1.7
* Node 0.12 or higher
* bower
* maven 3

## Installing frontend dependencies ##

After cloning the repository, the following command installs the Javascript/css dependencies:

* bower install

Task description:

Реализовать frontend для просмотра данной статистики 
Создать Hiberante mapping  для двух таблиц 
Сервер должен возвращать список   статистики по обработанным файлам, и статистику по строкам для выбранного файла. Можно использовать REST или Spring MVC(последний мы вроде уже не используем в Swissquote) 
Создать frontend часть для приложения - отобразить список статистики по файлам, при клике на строку с файлом отобразить статистику строк(JavaScript, jQuery etc) 

Опционально

Добавить на UI поиск по параметрам статистики(пример: файлы с количеством строк больше 10)
Добавить в UI приложение возможность ввести текст и оправить его на обработку