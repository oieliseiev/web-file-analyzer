package com.ieliseiev.service;

import com.ieliseiev.domain.ProcessString;

import java.util.List;

public interface StringService extends Service<ProcessString> {

    /**
     * Finds string for given file id.
     *
     * @param fileId to search
     * @return list of strings
     */
    List<ProcessString> findByFileId(Long fileId);
}
