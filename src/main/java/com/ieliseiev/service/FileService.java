package com.ieliseiev.service;

import com.ieliseiev.domain.ProcessFile;

public interface FileService extends Service<ProcessFile> {
}
