package com.ieliseiev.service;

import java.util.List;

public interface Service<T> {

    /**
     * Finds entity by given id.
     *
     * @param id to search
     * @return entity
     */
    T findById(Long id);

    /**
     * Finds all entities.
     *
     * @return list of entities
     */
    List<T> findAll();
}
