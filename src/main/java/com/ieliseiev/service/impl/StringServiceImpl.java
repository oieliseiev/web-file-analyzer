package com.ieliseiev.service.impl;

import com.ieliseiev.dao.StringDAO;
import com.ieliseiev.domain.ProcessString;
import com.ieliseiev.service.StringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class StringServiceImpl implements StringService {

    @Autowired
    private StringDAO stringDAO;

    @Override
    public ProcessString findById(Long id) {
        return stringDAO.findById(id);
    }

    @Override
    public List<ProcessString> findAll() {
        return stringDAO.findAll();
    }

    @Override
    public List<ProcessString> findByFileId(Long fileId) {
        return stringDAO.findByFileId(fileId);
    }
}
