package com.ieliseiev.service.impl;

import com.ieliseiev.dao.FileDAO;
import com.ieliseiev.domain.ProcessFile;
import com.ieliseiev.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class FileServiceImpl implements FileService {

    @Autowired
    private FileDAO fileDAO;

    @Override
    public ProcessFile findById(Long id) {
        return fileDAO.findById(id);
    }

    @Override
    public List<ProcessFile> findAll() {
        return fileDAO.findAll();
    }
}
