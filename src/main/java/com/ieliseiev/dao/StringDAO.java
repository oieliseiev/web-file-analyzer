package com.ieliseiev.dao;

import com.ieliseiev.domain.ProcessString;

import java.util.List;

public interface StringDAO extends BaseDAO<ProcessString> {

    /**
     * Finds all strings for given file id.
     *
     * @param fileId to search strings
     * @return list of strings
     */
    List<ProcessString> findByFileId(Long fileId);
}
