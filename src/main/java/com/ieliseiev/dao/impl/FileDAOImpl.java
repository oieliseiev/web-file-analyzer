package com.ieliseiev.dao.impl;

import com.ieliseiev.dao.FileDAO;
import com.ieliseiev.domain.ProcessFile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class FileDAOImpl implements FileDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public ProcessFile findById(Long id) {
        return em.find(ProcessFile.class, id);
    }

    @Override
    public List<ProcessFile> findAll() {
        return em.createQuery("from com.ieliseiev.domain.ProcessFile").getResultList();
    }
}
