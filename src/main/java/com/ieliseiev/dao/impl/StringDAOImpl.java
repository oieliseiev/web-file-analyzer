package com.ieliseiev.dao.impl;

import com.ieliseiev.dao.StringDAO;
import com.ieliseiev.domain.ProcessString;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class StringDAOImpl implements StringDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public ProcessString findById(Long id) {
        return em.find(ProcessString.class, id);
    }

    @Override
    public List<ProcessString> findAll() {
        return em.createQuery("from com.ieliseiev.domain.ProcessString").getResultList();
    }

    @Override
    public List<ProcessString> findByFileId(Long fileId) {
        return em.createQuery("from com.ieliseiev.domain.ProcessString f where f.processFile.fileId = :fileId")
                .setParameter("fileId", fileId)
                .getResultList();
    }
}
