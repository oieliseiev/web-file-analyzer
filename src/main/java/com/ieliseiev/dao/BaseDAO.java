package com.ieliseiev.dao;

import java.util.List;

public interface BaseDAO<T> {
    /**
     * Finds entity by given id.
     *
     * @param id to search file
     * @return file
     */
    T findById(Long id);

    /**
     * Finds all entities.
     *
     * @return list of entities
     */
    List<T> findAll();
}
