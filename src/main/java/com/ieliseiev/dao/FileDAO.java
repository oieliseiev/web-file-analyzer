package com.ieliseiev.dao;

import com.ieliseiev.domain.ProcessFile;

public interface FileDAO extends BaseDAO<ProcessFile> {
}
