package com.ieliseiev.controller;

import com.ieliseiev.domain.ProcessFile;
import com.ieliseiev.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProcessFile>> listAllFiles() {
        List<ProcessFile> files = fileService.findAll();
        if (files.isEmpty()) {
            return new ResponseEntity<List<ProcessFile>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ProcessFile>>(files, HttpStatus.OK);
    }
}
