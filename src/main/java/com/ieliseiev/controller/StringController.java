package com.ieliseiev.controller;

import com.ieliseiev.domain.ProcessString;
import com.ieliseiev.service.StringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/string")
public class StringController {

    @Autowired
    private StringService stringService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProcessString>> listStringsByFileId(@RequestParam("fileId") long fileId) {
        List<ProcessString> stringList = stringService.findByFileId(fileId);
        if (stringList.isEmpty()) {
            return new ResponseEntity<List<ProcessString>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ProcessString>>(stringList, HttpStatus.OK);
    }
}
