package com.ieliseiev.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PROCESS_FILE")
public class ProcessFile {

    private long fileId;
    private String name;
    private List<ProcessString> processStrings;
    private int stringCount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FILE_ID", unique = true, nullable = false)
    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "processFile")
    @JsonIgnore
    public List<ProcessString> getProcessStrings() {
        return processStrings;
    }

    public void setProcessStrings(List<ProcessString> processStrings) {
        this.processStrings = processStrings;
    }

    @Column(name = "STRING_COUNT")
    public int getStringCount() {
        return stringCount;
    }

    public void setStringCount(int stringCount) {
        this.stringCount = stringCount;
    }
}
