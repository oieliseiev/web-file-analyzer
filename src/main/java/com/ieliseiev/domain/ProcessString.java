package com.ieliseiev.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "PROCESS_STRING")
public class ProcessString {

    private long stringId;
    private String maxWord;
    private String minWord;
    private int length;
    private double averageWordLength;
    private ProcessFile processFile;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STRING_ID", unique = true, nullable = false)
    public long getStringId() {
        return stringId;
    }

    public void setStringId(long stringId) {
        this.stringId = stringId;
    }

    @Column(name = "MAX_WORD")
    public String getMaxWord() {
        return maxWord;
    }

    public void setMaxWord(String maxWord) {
        this.maxWord = maxWord;
    }

    @Column(name = "MIN_WORD")
    public String getMinWord() {
        return minWord;
    }

    public void setMinWord(String minWord) {
        this.minWord = minWord;
    }

    @Column(name = "LENGTH")
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Column(name = "AVERAGE_WORD_LENGTH")
    public double getAverageWordLength() {
        return averageWordLength;
    }

    public void setAverageWordLength(double averageWordLength) {
        this.averageWordLength = averageWordLength;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_ID")
    public ProcessFile getProcessFile() {
        return processFile;
    }

    public void setProcessFile(ProcessFile processFile) {
        this.processFile = processFile;
    }
}
