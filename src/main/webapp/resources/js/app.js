(function () {
    'use strict';
    var app = angular.module('analyzer', []);
    app.controller('fileController', function ($scope, $http) {
        $scope.fileItems = [];
        $scope.stringItems = [];
        $scope.selectedFile = "none";
        $scope.loadFiles = function () {
            $http({
                method: 'GET',
                url: '/file'
            }).then(function successCallback(response) {
                console.log("Files loaded: " + response.data.length);
                $scope.fileItems = response.data;
            }, function errorCallback(response) {
                console.error("Failed to get files: " + response)
            });
        };
        $scope.loadStrings = function (file) {
            console.log("Start loading strings by file id: " + file.fileId);
            $http({
                method: 'GET',
                url: '/string',
                params: {fileId: file.fileId}
            }).then(function successCallback(response) {
                console.log("Strings loaded: " + response.data.length);
                $scope.stringItems = response.data;
                $scope.selectedFile = file.name;
            }, function errorCallback(response) {
                console.error("Failed to get strings: " + response)
            });
        }
    });
})();